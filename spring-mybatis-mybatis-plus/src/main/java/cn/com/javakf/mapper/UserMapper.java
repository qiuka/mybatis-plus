package cn.com.javakf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.com.javakf.pojo.User;

public interface UserMapper extends BaseMapper<User> {

}
