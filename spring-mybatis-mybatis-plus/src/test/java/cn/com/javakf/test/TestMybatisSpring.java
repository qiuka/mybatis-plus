package cn.com.javakf.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.com.javakf.mapper.UserMapper;
import cn.com.javakf.pojo.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class TestMybatisSpring {

	@Autowired
	private UserMapper userMapper;

	@Test
	public void testSelectList() {
		List<User> users = this.userMapper.selectList(null);
		for (User user : users) {
			System.out.println(user);
		}
	}

}
