package cn.com.javakf.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.com.javakf.pojo.User;

public interface UserMapper extends BaseMapper<User> {

	List<User> findAll();

}
