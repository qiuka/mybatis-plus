package cn.com.javakf.test;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import cn.com.javakf.mapper.UserMapper;
import cn.com.javakf.pojo.User;

public class TestMybatis {

	@Test
	public void testFindAll() throws Exception {

		String config = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(config);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		SqlSession sqlSession = sqlSessionFactory.openSession();
		UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

		// 测试查询
		List<User> users = userMapper.findAll();
		for (User user : users) {
			System.out.println(user);
		}

	}

}
